import React from 'react';

import { State } from './context';

import Editor from './components/Editor/editor.component';
import Previewer from './components/Previewer/previewer.component';
import NavBar from './components/NavBar/navbar.component';


import './App.css';

function App() {

  return (
    <State>
      <div className="App">
        <header>
          <NavBar />
        </header>
        <main>
          <Editor />
          <Previewer />
        </main>
      </div>
    </State>
  );
}

export default App;
