import React, { useReducer, useRef } from "react";
import Context from "./context";
import Reducer from './reducer';

function State(props){
    
    const initialState = {
        filename: 'Untitled',
        theme: localStorage.getItem('theme') || 'light',
        editor: null,
        content: "",
        previewerRef: useRef()
    }

    const [state, dispatch] = useReducer(Reducer, initialState);

    const setFilename = (filename) => {
        dispatch({
            type: 'SET_FILENAME',
            payload: filename
        })
    }

    const setContent = (content) => {
        dispatch({
            type: 'SET_CONTENT',
            payload: content
        })
    }

    const setTheme = (theme) => {
        dispatch({
            type: 'SET_THEME',
            payload: theme
        })
        localStorage.setItem('theme', theme)
    }

    return (
        <Context.Provider value={{
            filename: state.filename,
            content: state.content,
            previewerRef: state.previewerRef,
            theme: state.theme,
            setFilename,
            setContent,
            setTheme
        }}>
            { props.children }
        </Context.Provider>
    )
}

export default State;