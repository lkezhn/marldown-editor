
export default function (state, action) {
    const { payload, type } = action

    switch (type) {
        case 'SET_FILENAME':
            return {
                ...state,
                filename: payload
            }
        case 'SET_CONTENT':
            return {
                ...state,
                content: payload
            }
        case 'SET_THEME':
            return {
                ...state,
                theme: payload
            }
        default: return state
    }

}