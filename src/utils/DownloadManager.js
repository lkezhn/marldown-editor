import htmlToPdf from 'html-to-pdfmake'
import pdfMake from 'pdfmake'
import pdfFonts from 'pdfmake/build/vfs_fonts';

class DownloadManager {

    /**
     * Permite descargar la previsualización del documento Markdown en formato PDF 
    */
    downloadPreviewAsPDF(content, filename) {

        if(filename === ""){
            filename = 'Untitled'
        }
        
        pdfMake.vfs = pdfFonts.pdfMake.vfs;

        const info = htmlToPdf(content, {
            imagesByReference: true,
            defaultStyles: {
                h1: {
                    fontSize: 16, bold: true, marginBottom: 5
                },
                h2: {
                    fontSize: 13, bold: true, marginBottom: 5
                },
                h3: {
                    fontSize: 11, bold: true, marginBottom: 5
                },
                th: {
                    fillColor: '#ffffff', alignment: 'center'
                }
            }
        });


        info.content.map((element) => {
            if (element.stack) {
                element.stack[0].maxWidth = 512
            }
        })

        const doc = {
            content: info.content,
            images: info.images,
            styles: {
                imgStyle: {
                    outerWidth: 10,
                    outerHeight: 10
                }
            },
            defaultStyle: {
                fontSize: 10,
            }
        }

        pdfMake.createPdf(doc).download(filename)
    }

    downloadFileAsMarkdown(filename, content) {
        
        if(filename === ""){
            filename = 'Untitled'
        }

        const element = document.createElement('a')
        const file = new Blob([content], {
            type: 'text/plain'
        });

        element.href = URL.createObjectURL(file)

        element.download = `${filename}.md`

        document.body.appendChild(element)
        element.click()
        document.body.removeChild(element)
    }

}

export default new DownloadManager();
