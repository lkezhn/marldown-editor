import React, { useContext } from "react";
import { Context } from "../../context";

import MarkdownDownloadButton from "../molecules/MarkdownDownloadButton.molecule";
import PdfDOownloadButton from "../molecules/PdfDownloadButton.molecule";

export default function NavBar() {

    const { setFilename, filename, theme, setTheme } = useContext(Context);

    const changeFilename = (e) => {
        e.preventDefault()
        setFilename(e.target.value)
    }

    const changeTheme = (e) => {
        e.preventDefault()
        let newTheme = 'light'
        
        if(theme === 'light'){
            newTheme = 'dark'
        }

        setTheme(newTheme)
    }

    return (
        <nav className={`nav-${theme}`}>
            <section>
                <input id='filename' value={filename} onChange={changeFilename} type="text" className={`filename-${theme}`} />.md
            </section>
            <section className='button-section'>
                <button className='btn-theme' onClick={changeTheme}><i className={`fas fa-lightbulb btn-${theme}`}></i></button>
                <PdfDOownloadButton />
                <MarkdownDownloadButton />
            </section>
        </nav>
    )
}