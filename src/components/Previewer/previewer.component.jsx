import React, { useContext } from "react";

import Markdown from "marked-react";

import { Context } from "../../context";

export default function Previewer(){

    const { content, previewerRef, theme } = useContext(Context)

    return (
        <div className={`previewer-${theme}`} ref={previewerRef}>
            <Markdown>{content}</Markdown>
        </div>
    )

}