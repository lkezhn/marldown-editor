import React, { useEffect, useRef, useContext} from "react";

import { markdown, markdownLanguage } from '@codemirror/lang-markdown'
import { languages } from '@codemirror/language-data'
import { EditorView } from "@codemirror/view";
import { useCodeMirror} from '@uiw/react-codemirror'

import { Context } from "../../context";

export default function Editor() {

    const { setContent, theme } = useContext(Context);

    const editor = useRef()

    const { setContainer } = useCodeMirror({
        container: editor.current,
        extensions:[ EditorView.lineWrapping ,markdown({ base: markdownLanguage, codeLanguages: languages})],
        theme: theme,
        placeholder: 'Write your MarkDown code here',
        height: '100%',
        width: '100%',
        onChange: (value, viewUpdate) => {
            setContent(value)
        }
    })

    useEffect( () => {
        if (editor.current){
            setContainer(editor.current);
        }
    }, [editor.current])

    return (
        <div id='editor' ref={editor} />
    );
}