import React, { useContext } from "react";
import { Context } from "../../context";

import DownloadManager from "../../utils/DownloadManager";

export default function MarkdownDownloadButton(){

    const { filename, content } = useContext(Context);

    const downloadMatkdown = (e) => {
        e.preventDefault();
        DownloadManager.downloadFileAsMarkdown(filename, content)
    }

    return (
        <button className='nav-btn' onClick={downloadMatkdown}>Download content as MD</button>
    )
}