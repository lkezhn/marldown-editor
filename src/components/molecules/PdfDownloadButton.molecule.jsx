import React, { useContext } from "react";

import { Context } from "../../context";

import DM from '../../utils/DownloadManager.js'

export default function PdfDOownloadButton() {

    const { filename, previewerRef } = useContext(Context);

    const downloadPDF = () => {
        const content = previewerRef.current.innerHTML
        DM.downloadPreviewAsPDF(content, filename)
    }

    return (
        <button className='nav-btn' onClick={downloadPDF}>Download Preview as PDF</button>
    )

}